/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef LARREADOUTGEOMETRY_EMBCELLCONSTLINK_H
#define LARREADOUTGEOMETRY_EMBCELLCONSTLINK_H
#include "GeoModelKernel/GeoIntrusivePtr.h"
#include "LArReadoutGeometry/EMBCell.h"

using EMBCellConstLink=GeoIntrusivePtr<const EMBCell>;

#endif
